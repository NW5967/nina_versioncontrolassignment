using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_PlayerController : MonoBehaviour
{
    /// <summary>
    /// This script will control the player
    /// </summary>


    //Reference to the Input_Action script
    private Input_Actions controls;

    //Reference to the Rigidbody2d
    private Rigidbody2D rb;

    //This variable will store the value of what button we have pressed (-1 if A, 1 if D)
    public float horizontalValue;

    //Variable to decide how fast the player movement is
    [SerializeField] private float speed;

    //Will be used to store the original scale of the player
    private Vector3 originalPlayerScale;

    //Will be used to flip the player
    private Vector3 scaledFlippedOnTheX;



    private void Awake()
    {
         //Creates a new Input_Actions object and then stores it in the controls variable
        //I need to do this in order for the custom input I created to be read
        controls = new Input_Actions();

        //Stores the Rigidbody2d into rb
        rb = GetComponent<Rigidbody2D>();

        //Fires everytime we are pressing any button in our Horizontal button scheme
        controls.Pc.Horizontal.performed += _ => MethodsFiredOnHorizontalPerformed();

        //Stores the player's scale in this variable
        originalPlayerScale = transform.localScale;

        //Flips the X and then stores everything in the variable
        scaledFlippedOnTheX = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
    }

    private void OnEnable()
    {
        //Enables controls when script is enabled
        controls.Enable();
    }
    private void OnDisable()
    {
        //Disables controls on script disable
        controls.Disable();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StoresPlayerHorizontalValue();
        MovePlayer();
    }

    void MethodsFiredOnHorizontalPerformed()
    {
        //Debug.log to control if method is firing (uncomment to run)
        //Debug.Log("method firing");

        FlipTheplayer();
    }

    void StoresPlayerHorizontalValue()
    {
        //Reads what button are we pressing, and assign its value (-1 if A, +1 if D) to the horizontalValue variable
        horizontalValue = controls.Pc.Horizontal.ReadValue<float>();
    }

    void MovePlayer()
    {
         //Creates a vector2 that stores the horizontalValue(-1 or +1), and multiplies it by speed and Time.Deltatime to make it framerate independent on the x
        // and zero in the y, since we just want to have horizontal force
        Vector2 force = new Vector2(horizontalValue * speed * Time.deltaTime, 0f);

        //Adds force to the player's rigidbody, using the variable we just created, in an Impulse fashion
        rb.AddForce(force, ForceMode2D.Impulse);
    }

    void FlipTheplayer()
    {
        //If we are pressing A it will be -1
        if (controls.Pc.Horizontal.ReadValue<float>() == -1)
        {
            //Flips the player on the X
            transform.localScale = scaledFlippedOnTheX;
        }
        else if (controls.Pc.Horizontal.ReadValue<float>() == 1)
        {
            //resets the scale to its original value
            transform.localScale = originalPlayerScale;

        }
    }
}
