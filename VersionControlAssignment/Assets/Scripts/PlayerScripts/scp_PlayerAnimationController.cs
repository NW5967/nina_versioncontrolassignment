using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_PlayerAnimationController : MonoBehaviour
{
    /// <summary>
    /// This script will eal with all the animations of the player
    /// </summary>
    

    //Reference to the player controller script
    scp_PlayerController player;

    //Reference to the animator
    Animator anim;

    private void Awake()
    {
        //Can use get component to find this script because it is on the same gameobject as this script
        player = GetComponent<scp_PlayerController>();

        //Get component will find the animator and store it in the anim variable
        anim = GetComponent<Animator>();
    }
    

    // Update is called once per frame
    void Update()
    {
        SetRunSpeedParameter();
    }

    void SetRunSpeedParameter()
    {
         //Converts the horizontal value from the player script in an absolute value
        //any value will be converset in a positive one, absolute value will not be negative
        float absoluteHorizontalValue = Mathf.Abs(player.horizontalValue);

        //Sets the RunSpeed parameter to be the same as our absoluteHorizontalValue, thus activating our run animation
        anim.SetFloat("RunSpeed", absoluteHorizontalValue);
    }
}
