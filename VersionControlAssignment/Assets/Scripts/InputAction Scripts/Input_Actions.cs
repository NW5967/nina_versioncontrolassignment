// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/Input_Actions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Input_Actions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Input_Actions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Input_Actions"",
    ""maps"": [
        {
            ""name"": ""Pc"",
            ""id"": ""6b308e04-9a40-46c6-95de-83c9b3d7b3a3"",
            ""actions"": [
                {
                    ""name"": ""Horizontal"",
                    ""type"": ""Button"",
                    ""id"": ""3a8425ee-0aea-4622-93fa-69876923814c"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Vertical"",
                    ""type"": ""Button"",
                    ""id"": ""2266ecdc-ec6a-47c4-bfe4-85d0a237a36b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""a7772c45-709d-4292-b0fc-6f077eb598bf"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""2bfe8fb5-8602-4658-807f-42301443dfca"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""4ba30f04-c2c5-4495-b56c-85b8f247a09f"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Horizontal"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""1D Axis"",
                    ""id"": ""cfe85051-4e03-4f6f-b1a3-0f999f5b561f"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Vertical"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""ec6e4706-0e5f-42d1-8567-05ed0f62bd0f"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""cecd77eb-d45d-4e78-a674-7ad12bf5c71a"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Vertical"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Pc
        m_Pc = asset.FindActionMap("Pc", throwIfNotFound: true);
        m_Pc_Horizontal = m_Pc.FindAction("Horizontal", throwIfNotFound: true);
        m_Pc_Vertical = m_Pc.FindAction("Vertical", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Pc
    private readonly InputActionMap m_Pc;
    private IPcActions m_PcActionsCallbackInterface;
    private readonly InputAction m_Pc_Horizontal;
    private readonly InputAction m_Pc_Vertical;
    public struct PcActions
    {
        private @Input_Actions m_Wrapper;
        public PcActions(@Input_Actions wrapper) { m_Wrapper = wrapper; }
        public InputAction @Horizontal => m_Wrapper.m_Pc_Horizontal;
        public InputAction @Vertical => m_Wrapper.m_Pc_Vertical;
        public InputActionMap Get() { return m_Wrapper.m_Pc; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PcActions set) { return set.Get(); }
        public void SetCallbacks(IPcActions instance)
        {
            if (m_Wrapper.m_PcActionsCallbackInterface != null)
            {
                @Horizontal.started -= m_Wrapper.m_PcActionsCallbackInterface.OnHorizontal;
                @Horizontal.performed -= m_Wrapper.m_PcActionsCallbackInterface.OnHorizontal;
                @Horizontal.canceled -= m_Wrapper.m_PcActionsCallbackInterface.OnHorizontal;
                @Vertical.started -= m_Wrapper.m_PcActionsCallbackInterface.OnVertical;
                @Vertical.performed -= m_Wrapper.m_PcActionsCallbackInterface.OnVertical;
                @Vertical.canceled -= m_Wrapper.m_PcActionsCallbackInterface.OnVertical;
            }
            m_Wrapper.m_PcActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Horizontal.started += instance.OnHorizontal;
                @Horizontal.performed += instance.OnHorizontal;
                @Horizontal.canceled += instance.OnHorizontal;
                @Vertical.started += instance.OnVertical;
                @Vertical.performed += instance.OnVertical;
                @Vertical.canceled += instance.OnVertical;
            }
        }
    }
    public PcActions @Pc => new PcActions(this);
    public interface IPcActions
    {
        void OnHorizontal(InputAction.CallbackContext context);
        void OnVertical(InputAction.CallbackContext context);
    }
}
