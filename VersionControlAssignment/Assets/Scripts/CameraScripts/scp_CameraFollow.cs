using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scp_CameraFollow : MonoBehaviour
{
    /// <summary>
    /// This script will make sure that the camera follows the player.
    /// </summary>

    //Will be used to set the target for the camera
    [SerializeField]private Transform target;

    //Value to give the camera a Z offsett
    Vector3 cameraZOffsett =  new Vector3(0f, 3.111225f, -10f);

    //Value to control how fast the camera will be at going to the player positions
    [Range(1, 10)]
    [SerializeField]float smoothValue = 1;

    // Start is called before the first frame update
    void Start()
    {
        //Sets the target transform to be the same as the player's one
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {
        CameraFollow();
    }

    void CameraFollow()
    {
         //Will create a variable that stores the target's transform with some offsett
        //The offset is needed to make sure that the camera will not clip (overlap with the player's position, thus not showing the player correctly)
        Vector3 targetPosition = target.position + cameraZOffsett;

         //The Vector3.Lerp will make sure there is a transition between wwhere the camera is now (transform.position),
        //where the camera should go (targetPosition), and how smoothly shall it move
        Vector3 smoothPosition = Vector3.Lerp(transform.position, targetPosition, smoothValue * Time.deltaTime);

        //This line updates the actual camera position, to the smoothed one that we want
        Camera.main.transform.position = smoothPosition;
    }
}
